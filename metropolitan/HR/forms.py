from django import forms
from django.contrib.auth.models import User
#from authenticate.models import UserProfileInfo
from HR.models import Attendance1
from datetime import date
from authenticate.models import UserProfileInfo


class DateInput(forms.DateInput):
     input_type = 'date'

class AttendanceForm(forms.ModelForm):
    end_date = forms.DateField(widget=DateInput())
    description = forms.CharField(widget=forms.Textarea)
    class Meta():
        model = Attendance1
        #testt
        fields = '_all_'    


