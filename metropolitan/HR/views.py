from django.shortcuts import render
from django.views.generic import View, TemplateView, ListView, DetailView
import datetime
from django.http import HttpResponse
from HR.models import Attendance1 # Create your views here.
from authenticate.models import UserProfileInfo
from django.contrib.auth.decorators import login_required
#from .forms import AttendanceForm
from django.contrib.auth.models import User



@login_required
def index(request):

    return render(request, 'HR/HRchoice.html', )

# Create your views here.
@login_required
def display(request):
    Attendances1 = Attendance1.objects.all()
    
    '''
    if request.method == "POST":
        data = request.POST.get("TaskID")
        task_ = Task.objects.get(taskId = int(data))
        task_.status = "C"
        task_.save()
        #print(data)
    '''
#    Tasks = Task.objects.order_by('end_date')
    users2 = UserProfileInfo.objects.order_by().distinct()
    attendance_list = []
    for attendance in Attendances1:
        attendance_list.append(attendance)
    #print(attendance_list)
    args = {"attendance_list" : attendance_list,"users":users2}
    return render(request, 'HR/HR_main.html', args)

@login_required
def displayUser(request):
    print('in display user')
    print(request.POST)

    if request.method == "POST":
        print('in POST')

        print(request.POST.get("employee"))
        username = request.POST.get("employee")
        attendance_ = Attendance1.objects.get(employee = username)
        args = {"attendance_list" : attendance_}

        return render(request, 'HR/detailview.html', args)

def search(request):
   # users = UserProfileInfo.objects.exclude(user = request.user)
    #creator = UserProfileInfo.objects.get(id = request.user.id)
    args = {}
    users2 = UserProfileInfo.objects.all()
    args = {"user_info" : users2}

    if request.method == "POST":
        print( request.POST)
        users2 = UserProfileInfo.objects.all()
        data = request.POST
        assignee_ = data.get("user")
        print(assignee_)
        
        for u in users2:
            if str(u.user)  == assignee_:
                id1 = u.id
                break
        assignee_ = Attendance1.objects.filter(employee = id1)
        profile = UserProfileInfo.objects.get(id = id1)
        print(assignee_)
        status =[]
        for i in assignee_:
            status.append(i.status)
        details_user=abscensce_counter(status)    
        args = {"user_info" : profile,"abscence":details_user[0],"late":details_user[1] }
        return render(request, 'HR/records.html', args)
    print(args)
    return render(request, 'HR/detail.html', args)
class AttendanceDetailView(DetailView):
    
    model = Attendance1
    def get_context_data(self, **kwargs):

        context = super(AttendanceDetailView, self).get_context_data(**kwargs)

        context['attendance_list'] = Attendance1.objects.all()
        return context

def abscensce_counter(l):
    details=[]
    counter = 0
    counter2 = 0
    for i in l:
        if i =="A":
            counter+=1
        if i=="L":
            counter2+=1
    details.append(counter)
    details.append(counter2)
    return details