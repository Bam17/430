# Use include() to add paths from the catalog application 
from django.conf.urls import url
from . import views

app_name='HR'
urlpatterns = [
    url(r'^$', views.index, name='main'),
    url(r'^display/',views.display ,name='displayHR'),
    url(r'^displayUser/',views.displayUser ,name='displayDetail'),
   # url(r'^(?P<pk>[-\w]+)/$',views.AttendanceDetailView.as_view(),name='detail')
    url(r'^attendance_detail/',views.search, name="attendance_detail")
 
]