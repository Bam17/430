from django.db import models
from django.db import models
from authenticate.models import UserProfileInfo
from datetime import date

# Create your models here.
class Attendance1(models.Model):
    attendanceId = models.AutoField(primary_key=True)
    employee = models.ForeignKey(UserProfileInfo, on_delete=models.CASCADE, related_name='employee')  #default = request.user.is_authenticated
    date = models.DateField(default=date.today)
    timeIn = models.TimeField(auto_now=False, auto_now_add=False,null=True)
    timeOut = models.TimeField(auto_now=False, auto_now_add=False,null=True)
    status = models.CharField(max_length=1 , default = "N")
#test 3
    class Meta:
         unique_together = ('employee', 'date')

    def __str__(self):
        return self.employee.user.username
