from django.db import models
from datetime import date
from authenticate.models import UserProfileInfo
from datetime import datetime
# Create your models here.
class Task(models.Model):
    creator = models.ForeignKey(UserProfileInfo, on_delete=models.CASCADE, related_name='creator'  ) #default = request.user.is_authenticated
    taskId = models.AutoField(primary_key=True)
    assignee  = models.ForeignKey(UserProfileInfo, on_delete=models.CASCADE, related_name='assignee')
    description = models.CharField(max_length=400)
    start_date = models.DateField(default=datetime.now)
    end_date = models.DateField()
    status = models.CharField(max_length=1 , default = "I")
#test 2
#test 3
    def __str__(self):
        return str(self.taskId)