from django import forms
from django.contrib.auth.models import User
#from authenticate.models import UserProfileInfo
from task.models import Task
from datetime import date
from authenticate.models import UserProfileInfo


class DateInput(forms.DateInput):
     input_type = 'date'

class TaskForm(forms.ModelForm):
    end_date = forms.DateField(widget=DateInput())
    description = forms.CharField(widget=forms.Textarea)
    class Meta():
        model = Task
        #testt
        fields = ("assignee","description","end_date","creator")
    


