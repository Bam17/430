# Use include() to add paths from the catalog application 
from django.conf.urls import url
from task import views

app_name='task'
urlpatterns = [
    #path('catalog/', include('catalog.urls')),
    url(r'^$', views.tasks, name='tasks'),
    url(r'^display/',views.display ,name='display'),
    url(r'Assign/', views.assign,name='assign'),

]