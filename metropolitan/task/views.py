from django.shortcuts import render
import datetime
from django.http import HttpResponse
from task.models import Task # Create your views here.
from authenticate.models import UserProfileInfo
from django.contrib.auth.decorators import login_required
from .forms import TaskForm
from django.contrib.auth.models import User

@login_required
def tasks(request):

    return render(request, 'task/tasks.html', )


def assign(request):
    data = request.POST
    print(request.user)
    users = UserProfileInfo.objects.exclude(user = request.user)
    print("done")
    #creator = UserProfileInfo.objects.get(id = request.user.id)
    if request.method == "POST":
        users2 = UserProfileInfo.objects.all()
        data = request.POST
        end_date = data.get("end_date")
        assignee_ = data.get("dropdown")
        id1 = 0
        id2 = 0
        for u in users2:
            if str(u.user)  == assignee_:
                id1 = u.id
            elif str(u.user)  == request.user.username:
                id2 = u.id
        assignee = UserProfileInfo.objects.get(id = id1)
        creator = UserProfileInfo.objects.get(id = id2)
        description = data.get("description")
        creator = creator
        print(creator.user ,"of id :", id2 ,assignee_ , "of id :" , id1)
        Task.objects.create(end_date = end_date ,description = description ,status = "I" ,assignee = assignee , creator = creator)
        return render(request, 'task/tasks.html', {'users':users})
    return render(request, 'task/assign.html', {'users':users})
#test3
@login_required
def display(request):
    Tasks1 = Task.objects.all()
    if request.method == "POST":
        data = request.POST.get("TaskID")
        task_ = Task.objects.get(taskId = int(data))
        task_.status = "C"
        task_.save()
        #print(data)
    Tasks = Task.objects.order_by('end_date')
    task_list = []
    for task in Tasks:
        if str(task.assignee) == request.user.username and str(task.status) == "I": 
            task_list.append(task)
    args = {"task_list" : task_list}
    return render(request, 'task/display.html', args)

