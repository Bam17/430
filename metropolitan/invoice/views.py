from django.shortcuts import render,redirect
from . import forms
from invoice.models import Items, Address, Company, Invoice, invoice_items
from django.forms import Form
from authenticate.models import UserProfileInfo
from datetime import datetime
from django.forms import modelformset_factory

from .forms import ItemFormset


def add_id(address_form:Form):
    street =address_form.cleaned_data["street_name"]
    city = address_form.cleaned_data["city_name"]
    country = address_form.cleaned_data["country_name"]
    incident = Address.objects.filter(street_name=street,city_name=city, country_name=country )
    if not incident:
        address_form.save()
        incident = Address.objects.filter(street_name=street,city_name=city, country_name=country )
    for i in incident:
        return i

def comp_id(company_form:Form, add: Form, telephone)-> Form:
    company_n = company_form.cleaned_data["company_name"]
    company_e = company_form.cleaned_data["company_email"]
    company_t = telephone
    incident = Company.objects.filter(address_ID = add, company_name= company_n, company_email=company_e, company_tel = company_t)
    if not incident:
        #company_form.save(commit=False)incident = Address.objects.filter(street_name=street,city_name=city, country_name=country )
        company = Company()
        company.address_ID = add
        company.company_name = company_n
        company.company_email = company_e
        company.company_tel = company_t
        company.save()
        incident = Company.objects.filter(address_ID = add, company_name= company_n, company_email=company_e, company_tel = company_t)
    for i in incident:
        return i

def inv_id(invoice_form:Form, cadd: Form, uadd: Form)-> Form:
    inv = Invoice()
    inv.creator = uadd
    inv.companey_ID = cadd
    inv.tax = invoice_form.cleaned_data["tax"]
    inv.save()
    user1 = Invoice.objects.get(invoiceId = inv.invoiceId)
    return user1
    

def invoice(request):
    if request.method =='POST':
        #Header
        address_form = forms.Address_Form(request.POST, prefix="address_form" )
        company_form = forms.Company_Form(request.POST, prefix="company_form")
        invoice_form = forms.Invoice_Form(request.POST, prefix="invoice_form")
        #Items
        formset = ItemFormset(request.POST)
        print(formset.errors)
        telephone = request.POST.get("telNum")
        if request.POST.get("adddd"):
            render(request, 'invoice/invoice.html',{'formset':formset,'address_form': address_form, "company_form":company_form, "invoice_form":invoice_form} )
        else:
            if address_form.is_valid() and company_form.is_valid() and invoice_form.is_valid() and formset.is_valid():
                print("DONEEE")
                print("Company name: ",company_form.cleaned_data["company_name"])
                print("Company email: ",company_form.cleaned_data["company_email"])
                print("Company tel: ",telephone)
                print("Tax : ",invoice_form.cleaned_data["tax"])
                print("Street : ",address_form.cleaned_data["street_name"])
                print("City : ",address_form.cleaned_data["city_name"])
                print("Country : ",address_form.cleaned_data["country_name"])
                print("username : ", request.user.username)
                ############ User db
                users2 = UserProfileInfo.objects.all()
                id2 = 0
                for u in users2:
                    if str(u.user)  == request.user.username:
                        id2 = u.id
                assignee_instance = UserProfileInfo.objects.get(id = id2)
                #Check in Address db; if (street city country) name combination exist, else add one, and return the id return address_id
                address_instance= add_id(address_form)
                #Check in Company db; if (company name, address_id) exists, else add one and return company_id
                company_instance= comp_id(company_form, address_instance, telephone)
                #Insertto invoice db; using companyid and userid, return invoice_id
                invoice_instance= inv_id(invoice_form, company_instance, assignee_instance)
                #Loop over items
                c =[]
                i=0
                for form in formset:
                    #Check in items db; if item code not exist, create one
                    item_instance=""
                    #print(form.cleaned_data["itemcode"])
                    incident = Items.objects.filter(itemcode=form.cleaned_data.get('itemcode'))
                    if incident:
                        print("\nItem of code: {} already exist with name = {} and price = {}".format(form.cleaned_data.get('itemcode'),form.cleaned_data.get("item_name"),form.cleaned_data.get("itemprice")))
                        print("they'll be updated according to the input")
                        incident.update(item_name=form.cleaned_data.get("item_name"), itemprice=form.cleaned_data.get("itemprice"),item_desc = form.cleaned_data.get('item_desc'))
                    else:
                            itemm = Items()
                            itemm.item_name=form.cleaned_data.get("item_name")
                            itemm.itemcode=form.cleaned_data.get('itemcode')
                            itemm.itemprice = form.cleaned_data.get('itemprice')
                            itemm.item_desc = form.cleaned_data.get('item_desc')
                            print("\nAdded new Item of code: {} name = {} and price = {}".format(form.cleaned_data.get('itemcode'),form.cleaned_data.get("item_name"),form.cleaned_data.get("itemprice")))
                            itemm.save()
                    item_instance = Items.objects.get(itemcode=form.cleaned_data.get("itemcode"))
                    #Insert to invoice_items db; itemcode, invoice_id ....
                    inv_it = invoice_items()
                    inv_it.itemcode = item_instance
                    inv_it.invoiceId = invoice_instance
                    inv_it.quantity = form.cleaned_data.get("quantity")
                    inv_it.save()
                return pdf(request, invoice_instance.invoiceId)
    else:
        address_form = forms.Address_Form(prefix="address_form")
        company_form = forms.Company_Form(prefix="company_form")
        invoice_form = forms.Invoice_Form(prefix="invoice_form")
        formset = ItemFormset(request.GET or None)
    return render(request, 'invoice/invoice.html',{'formset':formset,'address_form': address_form, "company_form":company_form, "invoice_form":invoice_form} )
def inv_opt(request):
    return render(request, 'invoice/options.html')
def pdf(request, invoice_id=None ):
    invalid=""
    if request.POST:
        if invoice_id:
            invoice_id=invoice_id
        elif request.POST.get('inv_id'):
            invoice_id=request.POST.get('inv_id')
        try:
            invoice = Invoice.objects.get(invoiceId = invoice_id)
            company=invoice.companey_ID.company_name
            company_email=invoice.companey_ID.company_email
            company_tel=invoice.companey_ID.company_tel
            street=invoice.companey_ID.address_ID.street_name
            city=invoice.companey_ID.address_ID.city_name
            country=invoice.companey_ID.address_ID.country_name
            invoicenum = invoice.invoiceId
            date = invoice.invoicedate
            tax = invoice.tax
            name=invoice.creator.user.username
            inv_it = invoice_items.objects.filter(invoiceId =invoice)
            ll=[]
            index=1
            total=0
            for i in inv_it:
                item_list=[]
                item_list.append(index)
                index=index+1
                item_list.append(int(i.itemcode.itemcode))
                item_list.append(i.itemcode.item_name)
                item_list.append(i.itemcode.itemprice)
                item_list.append(int(i.quantity))
                total=total+i.itemcode.itemprice * i.quantity
                item_list.append(i.itemcode.itemprice * i.quantity)
                item_list.append(i.itemcode.item_desc)
                ll.append(item_list)
            print(ll)
            print(total)
            tax=round(total*(tax/100.0),2)
            print(tax)
            t = round(total-tax,2)
            print(t)
            print(company_tel)
            return render(request, 'invoice/pdf.html', {'name':name,'t':t,'total':total,'tax':tax,'company':company,'company_tel':company_tel,'company_email':company_email,'street':street,'city':city,'country':country, 'invoicenum':invoicenum, 'date': date, 'list':ll})
        except:
            invalid = "Invalid Invoice ID"
            
    invoice = Invoice.objects.all()
    ll=[]
    for i in invoice:
        item_list=[]
        item_list.append(i.companey_ID.company_name)
        item_list.append(i.invoicedate)
        item_list.append(i.creator.user.username)
        item_list.append(i.invoiceId)
        ll.append(item_list)
    return render(request, 'invoice/search.html', {'list':ll,"invalid":invalid})