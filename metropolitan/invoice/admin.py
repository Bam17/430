from django.contrib import admin
from .models import Items,  Address, Company, Invoice, invoice_items

# Register your models here.
admin.site.register(Items)
admin.site.register(Address)
admin.site.register(Company)
admin.site.register(Invoice)
admin.site.register(invoice_items)