# Generated by Django 3.0.3 on 2020-05-06 14:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invoice', '0002_auto_20200506_1448'),
    ]

    operations = [
        migrations.AddField(
            model_name='items',
            name='item_desc',
            field=models.CharField(default='Deafult description setted......', max_length=100),
            preserve_default=False,
        ),
    ]
