from django import forms
from django.forms import modelformset_factory, formset_factory
#from authenticate.models import UserProfileInfo
from invoice.models import Items, Address, Company, Invoice, invoice_items

#the fields attributes for forms.ModelFrom class will connect to our models
class Address_Form (forms.ModelForm):
    class Meta:
        model =Address
        fields = "__all__"
        widgets = {
            'street_name': forms.TextInput(attrs={'class': 'form-control form-control-lg', 'placeholder':"Street"}),
            'city_name': forms.TextInput(attrs={'class': 'form-control form-control-lg', 'placeholder':"City"}),
            'country_name': forms.TextInput(attrs={'class': 'form-control form-control-lg', 'placeholder':"Country"}),
        }
        #1. fields = "__all__" # grab all fields
        #2. exclude = ["field1, field2"] #exclude those fields from model
        #3. fields = ("field1", "field1") #specify  the fields manually

class Company_Form (forms.ModelForm):
    class Meta:
        model =Company
        exclude = ["address_ID",'company_tel']
        widgets = {
            'company_name': forms.TextInput(attrs={'class': 'form-control form-control-lg', 'placeholder':"Company"}),
            'company_email': forms.TextInput(attrs={'class': 'form-control form-control-lg', 'placeholder':"Email"}),
        }
        #1. fields = "__all__" # grab all fields
        #2. exclude = ["field1, field2"] #exclude those fields from model
        #3. fields = ("field1", "field1") #specify  the fields manually

class Invoice_Form (forms.ModelForm):
    class Meta:
        model =Invoice
        fields = ("tax",)
        widgets = {
            'tax': forms.NumberInput(attrs={'class': 'form-control form-control-lg', 'min': '0', 'placeholder':"TAX Number"}),
        }
        #1. fields = "__all__" # grab all fields
        #2. exclude = ["field1, field2"] #exclude those fields from model
        #3. fields = ("field1", "field1") #specify  the fields manually

class Items_Form(forms.ModelForm):
    class Meta:
        model =Items
        fields = "__all__"
        widgets = {
            'itemcode': forms.NumberInput(attrs={'class': 'form-control form-control-lg', 'min': '0', 'placeholder':"Item Code"}),
            'item_name': forms.TextInput(attrs={'class': 'form-control form-control-lg', 'placeholder':"Item iterator"}),
            'itemprice': forms.NumberInput(attrs={'class': 'form-control form-control-lg', 'min': '0', 'placeholder':"Price"}),
            'item_desc': forms.NumberInput(attrs={'class': 'form-control form-control-lg', 'placeholder':"Description"}),
        }

class Invoice_items_Form(forms.ModelForm):
    class Meta:
        model =invoice_items
        fields = ("quantity",)
        widgets = {
            'quantity': forms.NumberInput(attrs={'class': 'form-control form-control-lg', 'placeholder':"Quantity"}),
        }
class Itemss(forms.Form):
    itemcode = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control form-control-lg', 'min': '0', 'placeholder':"Item Code"}))
    item_name = forms.CharField(max_length=50,widget=forms.TextInput(attrs={'class': 'form-control form-control-lg', 'placeholder':"Item name"}))
    item_desc = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-lg', 'placeholder':"Description"}))
    itemprice = forms.FloatField(widget=forms.NumberInput(attrs={'class': 'form-control form-control-lg', 'min': '0', 'placeholder':"Price"}))
    quantity = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control form-control-lg', 'min': '0', 'placeholder':"Quantity"}))
ItemFormset = formset_factory(Itemss, extra=1)

'''ItemsFormset = modelformset_factory(
    Items,
    fields = "__all__",
    extra=1,
    widgets = {
            'itemcode': forms.NumberInput(attrs={'class': 'mmw form-control', 'placeholder':"Item Code"}),
            'item_name': forms.TextInput(attrs={'class': 'mmw1 form-control', 'placeholder':"Item iterator"}),
            'itemprice': forms.NumberInput(attrs={'class': 'mmw3 form-control', 'placeholder':"Price/unit"}),
        }
)
VTFormset = modelformset_factory(
    invoice_items,
    fields = ("quantity",),
    extra=2,
    widgets = {
            'quantity': forms.NumberInput(attrs={'class': 'mmw2 form-control', 'placeholder':"Quantity"}),
        }
)'''
'''def add_ItemsFormset(InputSet: modelformset_factory):
    i = InputSet.total_form_count()
    newformset =  modelformset_factory(
    Items,
    fields = "__all__",
    extra=i+1,
    widgets = {
            'itemcode': forms.NumberInput(attrs={'class': 'mmw form-control', 'placeholder':"Item Code"}),
            'item_name': forms.TextInput(attrs={'class': 'mmw1 form-control', 'placeholder':"Item iterator"}),
            'itemprice': forms.NumberInput(attrs={'class': 'mmw3 form-control', 'placeholder':"Price/unit"}),
        }
    )
    newformset = newformset(queryset=Items.objects.none())
    field_old = []
    k=0
    for field in InputSet:
        field_old.append(field)
    for field in newformset:
        field = field_old[k]
        k+=1
        if k>=i:
            break
    return newformset'''