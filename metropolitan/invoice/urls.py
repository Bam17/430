# Use include() to add paths from the catalog application 
from django.conf.urls import url
from invoice import views

app_name='invoice'
urlpatterns = [
    #path('catalog/', include('catalog.urls')),
    url(r'^Generate', views.invoice, name='invoice'),
    url(r'^pdf', views.pdf, name='pdf'),
    url(r'$', views.inv_opt, name='inv_opt'),
]