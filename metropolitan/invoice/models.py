from django.db import models
from authenticate.models import UserProfileInfo
from datetime import datetime

class Items(models.Model):
    '''
    Items DB:
        itemcode      Primary key
        itemprice
    '''
    itemcode = models.IntegerField(primary_key=True)
    item_name = models.CharField(max_length=50)
    item_desc = models.CharField(max_length=100)
    itemprice = models.FloatField()

class Address(models.Model):
    '''
    address_ID   Primary key
    street_name
    country_name
    city_name
    '''
    address_ID = models.AutoField(primary_key=True)
    street_name = models.CharField(max_length=50)
    city_name = models.CharField(max_length=50)
    country_name = models.CharField(max_length=50)

class Company(models.Model):
    '''
    Company DB:
        companey_ID  Primary key
        company_name
        company_email
        company_tel
        address_ID   Foriegn key
    '''
    address_ID = models.ForeignKey(Address, on_delete=models.CASCADE)
    company_name = models.CharField(max_length=50)
    company_email = models.EmailField(max_length=50)
    company_tel = models.CharField(max_length=50)
    companey_ID = models.AutoField(primary_key=True)

class Invoice(models.Model):
    '''
    Invoice DB:
        UserID      foreign key
        companey_ID  foreign key
        invoiceID   Primary key
        tax
        invoicedate default today
    '''
    creator = models.ForeignKey(UserProfileInfo, on_delete=models.CASCADE)
    companey_ID = models.ForeignKey(Company, on_delete=models.CASCADE)
    invoiceId = models.AutoField(primary_key=True)
    tax = models.FloatField()
    invoicedate = models.DateField(default=datetime.now, blank=True)

class invoice_items(models.Model):
    '''
    invoice_items DB:
        itemcode      foreign key 
        invoiceID   foreign key
        invoice_items primary key
        quantity
    '''
    itemcode = models.ForeignKey(Items, on_delete=models.CASCADE)
    invoiceId = models.ForeignKey(Invoice, on_delete=models.CASCADE)
    invoice_items = models.AutoField(primary_key=True)
    quantity = models.IntegerField()